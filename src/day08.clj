(ns aoc2022.day08
  (:require [clojure.string :as str]
            [com.rpl.specter :refer :all]))

(defn parse [input]
  (->> input str/split-lines
       (map #(str/split % #""))
       (transform [ALL ALL] parse-long)))

(defn visible? [input]
  (let [process (fn [x]
                  (->> x (reduce
                          (fn [s i]
                            (let [[m r] (select MAP-VALS s)]
                              (if (< m i)
                                {:max i :result (conj r true)}
                                {:max m :result (conj r false)})))
                          {:max -1 :result []})
                       (select :result)))
        [a b] [input (reverse input)]
        a (flatten (process a))
        b (reverse (flatten (process b)))]
    (->> [a b]
         (apply map list)
         (map #(or (first %) (last %))))))

(defn scenic-score [input]
  (let [process (fn [t h]
                  (loop [trees t
                         result 0]
                    (if-not (empty? trees)
                      (if (>= (first trees) h)
                        (inc result)
                        (recur (rest trees) (inc result)))
                      result)))]
    (reduce (fn [result i]
              (let [[a b] (split-at i input)
                    val (first b)
                    b (process (rest b) val)
                    a (process (reverse a) val)]
                (conj result(reduce * [a b]))))
            []
            (range (count input)))))

(defn part1 [input]
  (let [input (parse input)
        rows (->> (map visible? input) (apply map list))
        cols (->> (apply map list input) (map visible?))
        matrix (->> (map list rows cols)
                    (transform [ALL] #(apply map list %))
                    (transform [ALL ALL] #(or (first %) (last %)))
                    (transform [ALL ALL] {true 1 false 0}))]
    (->> matrix flatten (reduce +))))

(defn part2 [input]
  (let [input (parse input)
        rows (->> (map scenic-score input) (apply map list))
        cols (->> (apply map list input) (map scenic-score))
        matrix (->> (map list rows cols)
                    (transform [ALL] #(apply map list %))
                    (transform [ALL ALL] #(reduce * %)))]
    (->> matrix flatten sort last)))

(map #(% (slurp "./data/day08.txt")) [part1 part2])
