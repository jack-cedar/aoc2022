(ns aoc2022.day07
  (:require [clojure.string :as str]
            [com.rpl.specter :as s]))

(defn make-tree [input]
  (->> (str/split-lines input)
       (reduce (fn [fs line]
                 (let [path (get fs :path)
                       tree (get fs :tree)
                       [_ ls] (re-find #"[$] ls" line)
                       [_ cd] (re-find #"[$] cd (.+)" line)
                       [_ dir] (re-find #"dir (.+)" line)
                       [_ size file] (re-find #"(\d+) (.+)" line)]
                   (cond
                     dir (s/setval :tree (assoc-in tree (conj path dir) {}) fs)
                     file (s/setval :tree (assoc-in tree (conj path file) (parse-long size)) fs)
                     cd (case cd
                          ".." (s/setval :path (pop path) fs)
                          (s/setval :path (conj path cd) fs))
                     :else fs)))
               {:path [] :tree {}})
       (s/select [:tree]) first))

(defn tree->sizeMap [input]
  (let [result (new java.util.HashMap)
        collect (fn collect [path dir]
                  (let [size
                        (transduce
                         (map (fn [[id size|dir]]
                                (if (int? size|dir)
                                  size|dir
                                  (collect (conj path id) size|dir))))
                         + 0 dir)]
                    (. result put path size) size))]
    (collect [] input)
    (into {} result)))

(defn solve [input]
  (let [sizes (->> input make-tree tree->sizeMap)
        part1 (reduce + (s/select [(s/walker int?) (s/pred<= 100000)] sizes))
        part2 (apply min (s/select [(s/walker int?)
                                    (s/pred>= (- (sizes []) 40000000) )]
                                   sizes))]
    {:part1 part1 :part2 part2}))

(solve (slurp "./data/day07.txt"))
