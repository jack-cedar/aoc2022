(ns aoc2022.day09
  (:require [clojure.string :as str]
            [com.rpl.specter :refer :all]))

(defn touching? [head tail]
  (let [[hx hy] head 
        [tx ty] tail]
    (and (>= 1 (abs (- hx tx)))
         (>= 1 (abs (- hy ty))))))

(defn move [rope dir]
  (let [[dx dy] dir
        [hx hy] (get rope :head)
        [tx ty] (get rope :tail)
        [hx hy] [(+ hx dx) (+ hy dy)]
        head [hx hy]
        tail [tx ty]]
    (if-not (touching? head tail)
      {:head head :tail [(- hx dx) (- hy dy)]}
      {:head head :tail tail})))

(defn tail-visited [rope dir n]
  (loop [rope rope
         result []]
    (if (= n (count result))
     {:rope rope :visited (conj result (get rope :tail))}
      (recur (case dir
               "U" (move rope [0 1])
               "D" (move rope [0 -1])
               "L" (move rope [-1 0])
               "R" (move rope [1 0]))
             (conj result (get rope :tail))))))

(defn part1 [input]
  (->> (reduce
        (fn [result step]
          (let [[_ dir n] step
                rope (get result :rope)
                visited (get result :visited)
                result (tail-visited rope dir (parse-long n))]
            {:rope (get result :rope) :visited (conj visited (get result :visited))}))
        {:rope {:head [0 0] :tail [0 0]} :visited []}
        (re-seq #"(L|R|U|D) (\d+)" input))
       (select [:visited ALL ALL])
       distinct
       count))
    
(part1 (slurp "./data/day09.txt"))
