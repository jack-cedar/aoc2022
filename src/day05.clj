(ns aoc2022.day05
  (:require
   [clojure.string :as str]
   [clojure.edn :as edn]))

(defn parse [input]
  (let [input (->> (str/split input #"\n\n")
                   (map str/split-lines))
        layout (->> (first input)
                    drop-last
                    (map #(re-seq #"    |\[(.*?)\]" %))
                    (map #(map last %))
                    (apply map list)
                    (map #(remove nil? %)))
        
        steps (->> (second input)
                   (map #(re-seq #"\d+" %))
                   (map #(map edn/read-string %))
                   (map (fn [x] (conj (->> (rest x) (map #(- % 1))) (first x) ))))]
    [layout steps]))

(defn process [part input]
  (let [input  (parse input)
        layout (first input)
        steps  (last input)]
    (->> (reduce (fn [result step]
                   (let [from      (nth result (second step))
                         to        (nth result (last step))
                         times     (first step)
                         newResult (assoc (vec result) (last step)
                                          (case part
                                            :part1 (->> (take times from) reverse (conj to) flatten)
                                            :part2 (->> (take times from) (conj to) flatten)))
                         newResult (assoc newResult (second step) (->> (drop times from)))]
                     newResult)) layout steps)
         (map first) (str/join))))

(process :part1 (slurp "./data/day05.dat"))
(process :part2 (slurp "./data/day05.dat"))
