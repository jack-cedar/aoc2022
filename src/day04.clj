(ns aoc2022.day03
  (:require
   [clojure.string :as str]
   [clojure.edn :as edn]))

(defn subseq? [a b]
  (some #{a} (partition (count a) 1 b)))

;; parses input into lists of ranges
;; Example: "2-4,6-8" -> (((2 3 4) (6 7 8)))
(defn parse [input]
  (->> (str/split-lines input)
       (map  (fn [x]
               (->> (str/split x #",")
                    (map #(str/split % #"-"))
                    (map #(map edn/read-string %))
                    (map #(range (first %) (inc (last %))))
                    )))))

(defn part1 [input]
  (->> (parse input)
       (map (fn [x]
              (cond
                (subseq? (first x) (last x)) 1
                (subseq? (last x) (first x)) 1
                :else nil)))
       (remove nil?)
       count))

(defn part2 [input]
  (->> (parse input)
       (map (fn [x] (apply distinct? (concat (first x) (last x)))))
       (remove true?)
       count))

(part1 (slurp "./data/day04.dat"))
(part2 (slurp "./data/day04.dat"))
