(ns aoc2022.day06)

(defn process [size input]
  (->> (partition size 1 input)
       (map #(apply distinct? %))
       (take-while false?) count
       (+ size)))

(println "Part1: %s%n" (process 4 (slurp "./data/day06.dat")))
(println "Part2: %s%n" (process 14 (slurp "./data/day06.dat")))   