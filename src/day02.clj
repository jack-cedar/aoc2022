(ns aoc2022.day02
  (:require
   [clojure.string :as str]
   [com.rpl.specter :as sp]))

(defn process [input]
  (let [
        values {"A" 1 "X" 1 "B" 2 "Y" 2 "C" 3 "Z" 3}
        [a b c] input
        [a b] [(get values a) (get values b)]
        [a b] (case c
                :L (if (= a 1) [1 3] [a (dec a)])
                :W (if (= a 3) [3 1] [a (inc a)])
                :T [a a] [a b])]
    (cond (= a b) (+ 3 a)
          (and (> a b) (= a 3) (= b 1)) (+ 6 b)
          (and (< a b) (= a 1) (= b 3)) (+ 0 b)
          (> a b) (+ 0 b) (< a b) (+ 6 b))))

(defn solve [input]
  (let [input (->> (str/split-lines input) (map #(str/split % #" ")))
        part2 (map #(sp/setval [2] (get {"X" :L "Y" :T "Z" :W } (second %)) %) input)
        solutions (sp/transform [sp/ALL sp/ALL] process [input part2])]
    (map hash-map [:part1 :part2] (map #(reduce + %) solutions))))

(solve (slurp "./data/day02.dat"))
