(ns aoc2022.day03
  (:require
   [clojure.string :as str]))

(defn get-priority [item]
  (let [item (int (.charAt item 0))]
    (if (< 96 item) (- item 96) (- item 38))))

(defn parse [input]
  (->> (str/split-lines input)
       (map #(str/split % #""))))

(defn part01 [input]
  (->> (parse input)
       (map #(partition (/ (count %) 2) %))
       (map #(map sort %))
       (map (fn [x]
              (->> x
                   (map distinct)
                   flatten
                   frequencies
                   (remove #(= (last %) 1))
                   (map first)
                   (map get-priority))))
       flatten
       (reduce +)))

(defn part02 [input]
  (->> (parse input)
       (partition 3)
       (map (fn [x]
              (->> (concat (->> (map #(distinct %) x)
                                rest
                                flatten
                                frequencies
                                (remove #(= (last %) 1))
                                (map first)) (distinct (first x)))
                   frequencies
                   (remove #(= (last %) 1))
                   (map first)
                   (map get-priority))))
       flatten
       (reduce +)))

(part01 (slurp "./data/day03.txt"))
(part02 (slurp "./data/day03.txt"))
