(ns aoc2022.day01
  (:require
   [clojure.string :as str]
   [com.rpl.specter :refer :all ]))

(defn parse [input]
  (->> (str/split input #"\n\n")
       (map #(str/split % #"\n"))
       (transform [ALL ALL] parse-long)
       (map #(reduce + %))))

(defn solve [input]
  (let [input (parse input)
        part1 (reduce max input)
        part2 (->> input sort
                   (take-last 3)
                   (reduce +))]
    {:part1 part1 :part2 part2}))

(solve (slurp "./data/day01.txt"))
